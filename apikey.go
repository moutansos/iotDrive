package main

//TODO: Add request counter to count up how many requests total each key has used.
// 		Maybe add a name field as well. When this changes, the database also needs to change
type APIKey struct {
	ID      int    `json:"id"`
	KeyVal  string `json:"keyval"`
	Enabled bool   `json:"enabled"`
}

type APIKeys []*APIKey
