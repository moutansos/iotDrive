package main

import (
	"encoding/json"
	"net/http"

	"io"
	"io/ioutil"

	"strconv"

	"errors"

	"log"

	"github.com/gorilla/mux"
)

const UNPROCESABLEENTITY int = 422

func Index(w http.ResponseWriter, r *http.Request) {
	var indexData = struct {
		ServerType string `json:"serverType"`
	}{"iotDrive"}
	EncodeAndSend(indexData, http.StatusOK, w)
}

//TODO: Throw error when can't get any data from database
func TodoIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	todos, err := DbFindAllTodo()
	if err != nil {
		panic(err)
	}

	err = json.NewEncoder(w).Encode(todos)

	if err != nil {
		panic(err)
	}
}

func AlarmIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	alarms, err := DbFindAllAlarm()
	if err != nil {
		panic(err)
	}

	err = json.NewEncoder(w).Encode(alarms)

	if err != nil {
		panic(err)
	}
}

func TodoShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId := vars["todoId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	id, _ := strconv.Atoi(todoId)
	t, err := DbFindTodo(id)
	if err != nil {
		SendAndLogError(err, "Error getting data from database", http.StatusInternalServerError, w)
	} else {
		w.WriteHeader(http.StatusOK)

		err := json.NewEncoder(w).Encode(t)
		if err != nil {
			SendAndLogError(err, "Error encoding the object from database", http.StatusInternalServerError, w)
		}
	}
}

func TodoCreate(w http.ResponseWriter, r *http.Request) {
	var todo Todo
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &todo); err != nil {
		SendAndLogError(err, "Unable to parse the object", UNPROCESABLEENTITY, w)
	} else {
		t, err := DbCreateTodo(&todo)
		if err != nil {
			SendAndLogError(err, "Unable to create Todo in database", http.StatusInternalServerError, w)
		} else {
			EncodeAndSend(t, http.StatusOK, w)
		}
	}
}

func AlarmCreate(w http.ResponseWriter, r *http.Request) {
	var alarm Alarm
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &alarm); err != nil {
		SendAndLogError(err, "Unable to parse the alarm.", UNPROCESABLEENTITY, w)
	} else {
		t, err := DbCreateAlarm(&alarm)
		if err != nil {
			SendAndLogError(err, "Unable to create alarm in database.", http.StatusInternalServerError, w)
		} else {
			EncodeAndSend(t, http.StatusCreated, w)
		}
	}
}

func AlarmDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	alarmId := vars["alarmId"]

	id, _ := strconv.Atoi(alarmId)

	keyStr := r.Header.Get("APIKey")
	if len(keyStr) == 0 {
		err := errors.New("no APIKey value set")
		SendAndLogError(err, "You must specify the APIKey value in the request header", UNPROCESABLEENTITY, w)
		return // Break execution on error
	} else {
		if err := AuthorizeKey(keyStr); err != nil {
			SendAndLogError(err, "Unauthorized API key provided", http.StatusForbidden, w)
			return
		}
		//AUTHORIZED:

		if err := DbDestroyAlarm(id); err != nil {
			SendAndLogError(err, "Unable to remove from database", http.StatusInternalServerError, w)
		}

	}
}

func EncodeAndSend(data interface{}, code int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(data); err != nil {
		SendAndLogError(err, "Unable to encode entity", http.StatusInternalServerError, w)
	}
}

func SendAndLogError(err error, msg string, code int, w http.ResponseWriter) {
	log.Print(err)

	data := struct {
		IsError bool   `json:"isError"`
		Message string `json:"message"`
		Err     error  `json:"error"`
	}{true, msg, err}

	EncodeAndSend(data, code, w)
}
