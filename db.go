package main

import (
	"database/sql"

	"github.com/lib/pq"
	_ "github.com/lib/pq"
)

var connStr string

const todoTableCreateCommand = `CREATE TABLE IF NOT EXISTS public."Todo"
								 (
									 "ID" BIGSERIAL NOT NULL,
									 "Name" text,
									 "Completed" boolean,
									 "Due" text,
									 PRIMARY KEY ("ID")
								 )`
const apiKeyTableCreateCommand = `CREATE TABLE IF NOT EXISTS public."APIKey"
								  (
									  "ID" BIGSERIAL NOT NULL,
									  "KeyVal" text,
									  "Enabled" boolean,
									  PRIMARY KEY ("ID")
								  )`
const alarmTableCreateCommand = `CREATE TABLE IF NOT EXISTS public."Alarm"
								 (
									 "ID" BIGSERIAL NOT NULL,
									 "GUID" text,
									 "DaysOfWeek" text[],
									 "EveryDay" boolean,
									 "Hour" SMALLINT,
									 "Minute" SMALLINT,
									 PRIMARY KEY ("ID")
								 )`
const queryTodoByID = `SELECT "ID", "Name", "Completed", "Due" FROM public."Todo" WHERE "ID" = $1`
const queryAPIKeyByID = `SELECT "ID", "KeyVal", "Enabled" FROM public."APIKey" WHERE "ID" = $1`
const queryAPIKeyByKeyVal = `SELECT "ID", "KeyVal", "Enabled" FROM public."APIKey" WHERE "KeyVal" = $1`
const queryAlarmByID = `SELECT "ID", "GUID", "DaysOfWeek", "EveryDay", "Hour", "Minute", FROM public."Alarm" WHERE "ID" = $1`
const insertTodoAndReturnID = `INSERT INTO public."Todo"("Name", "Completed", "Due") VALUES($1, $2, $3) RETURNING "ID"`
const insertAPIKeyAndReturnID = `INSERT INTO public."APIKey"("KeyVal", "Enabled") VALUES($1, $2) RETURNING "ID"`
const insertAlarmAndReturnID = `INSERT INTO public."Alarm"("GUID", "DaysOfWeek", "EveryDay", "Hour", "Minute") VALUES($1, $2, $3, $4, $5) RETURNING "ID"`
const queryAllTodos = `SELECT "ID", "Name", "Completed", "Due" FROM public."Todo"`
const queryAllAPIKeys = `SELECT "ID", "KeyVal", "Enabled" FROM public."APIKey"`
const queryAllAlarms = `SELECT "ID", "GUID", "DaysOfWeek", "EveryDay", "Hour", "Minute" FROM public."Alarm"`
const deleteTodoByID = `DELETE FROM public."Todo" WHERE "ID" = $1`
const deleteAPIKeyByID = `DELETE FROM public."APIKey" WHERE "ID" = $1`
const deleteAlarmByID = `DELETE FROM public."Alarm" WHERE "ID" = $1`

// DbInit initializes the database
func DbInit(connectionString string) error {
	connStr = connectionString
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}
	defer db.Close()

	//Create the todo table if it doesn't exist
	_, err = db.Exec(todoTableCreateCommand)
	if err != nil {
		return err
	}

	//Create the APIKey table if it doesn't exist
	_, err = db.Exec(apiKeyTableCreateCommand)
	if err != nil {
		return err
	}

	//Create the Alarms table if it doesn't exist
	_, err = db.Exec(alarmTableCreateCommand)
	if err != nil {
		return err
	}

	return nil
}

func DbFindTodo(id int) (*Todo, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	items, queryErr := db.Query(queryTodoByID, id)
	if queryErr != nil {
		return nil, queryErr
	}
	defer items.Close()

	for items.Next() {
		t := new(Todo)
		items.Scan(&t.Id, &t.Name, &t.Completed, &t.Due)
		if t.Id == id {
			return t, nil
		}
	}
	itemErr := items.Err()
	if itemErr != nil {
		return nil, itemErr
	}

	//Return nil if not found
	return nil, nil
}

//TODO: Test this
func DbFindAPIKeyById(id int) (*APIKey, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	items, err := db.Query(queryAPIKeyByID, id)
	if err != nil {
		return nil, err
	}
	defer items.Close()

	for items.Next() {
		k := new(APIKey)
		items.Scan(&k.ID, &k.KeyVal, &k.Enabled)
		if k.ID == id {
			return k, nil
		}
	}
	err = items.Err()
	if err != nil {
		return nil, err
	}

	//Return nil if not found
	return nil, nil
}

//TODO: Test this
func DbFindAPIKeyByKeyVal(keyVal string) (*APIKey, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	items, err := db.Query(queryAPIKeyByKeyVal, keyVal)
	if err != nil {
		return nil, err
	}
	defer items.Close()

	for items.Next() {
		k := new(APIKey)
		items.Scan(&k.ID, &k.KeyVal, &k.Enabled)
		if k.KeyVal == keyVal {
			return k, nil
		}
	}
	err = items.Err()
	if err != nil {
		return nil, err
	}

	//Return nil if not found
	return nil, nil
}

func DbFindAlarmByID(id int) (*Alarm, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	items, err := db.Query(queryAlarmByID, id)
	if err != nil {
		return nil, err
	}
	defer items.Close()

	for items.Next() {
		a := new(Alarm)
		items.Scan(&a.ID, &a.GUID, pq.Array(&a.DaysOfWeek), &a.EveryDay, &a.Hour, &a.Minute)
		if a.ID == id {
			return a, nil
		}
	}
	err = items.Err()
	if err != nil {
		return nil, err
	}

	//Return nil if not found
	return nil, nil
}

func DbFindAllTodo() (Todos, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	var todos Todos = []*Todo{}

	items, queryErr := db.Query(queryAllTodos)
	if queryErr != nil {
		return nil, queryErr
	}
	defer items.Close()

	for items.Next() {
		t := new(Todo)
		items.Scan(&t.Id, &t.Name, &t.Completed, &t.Due)
		todos = append(todos, t)
	}
	itemErr := items.Err()
	if itemErr != nil {
		return nil, itemErr
	}

	return todos, nil
}

//TODO: Test this
func DbFindAllAPIKey() (APIKeys, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	var apiKeys APIKeys = []*APIKey{}

	items, err := db.Query(queryAllAPIKeys)
	if err != nil {
		return nil, err
	}
	defer items.Close()

	for items.Next() {
		k := new(APIKey)
		items.Scan(&k.ID, &k.KeyVal, &k.Enabled)
		apiKeys = append(apiKeys, k)
	}
	err = items.Err()
	if err != nil {
		return nil, err
	}

	return apiKeys, nil
}

func DbFindAllAlarm() (Alarms, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	var alarms = []*Alarm{}

	items, err := db.Query(queryAllAlarms)
	if err != nil {
		return nil, err
	}
	defer items.Close()

	for items.Next() {
		a := new(Alarm)
		err := items.Scan(&a.ID, &a.GUID, pq.Array(&a.DaysOfWeek), &a.EveryDay, &a.Hour, &a.Minute)
		if err != nil {
			return alarms, err
		}
		alarms = append(alarms, a)
	}
	err = items.Err()
	if err != nil {
		return nil, err
	}

	return alarms, nil
}

func DbCreateTodo(t *Todo) (*Todo, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	err = db.QueryRow(insertTodoAndReturnID, t.Name, t.Completed, t.Due).Scan(&t.Id)
	if err != nil {
		return nil, err
	}
	return t, nil
}

//TODO: Test
func DbCreateAPIKey(k *APIKey) (*APIKey, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	err = db.QueryRow(insertAPIKeyAndReturnID, k.KeyVal, k.Enabled).Scan(&k.ID)
	if err != nil {
		return nil, err
	}
	return k, nil
}

func DbCreateAlarm(a *Alarm) (*Alarm, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	err = db.QueryRow(insertAlarmAndReturnID,
		a.GUID,
		pq.Array(a.DaysOfWeek),
		a.EveryDay,
		a.Hour,
		a.Minute).Scan(&a.ID)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func DbDestroyTodo(id int) error {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}

	_, err = db.Exec(deleteTodoByID, id)
	if err != nil {
		return err
	}
	return nil
}

//TODO: Test
func DbDestroyAPIKey(id int) error {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(deleteAPIKeyByID, id)
	if err != nil {
		return err
	}
	return nil
}

func DbDestroyAlarm(id int) error {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(deleteAlarmByID, id)
	if err != nil {
		return err
	}
	return nil
}
