package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	configPath := ""
	for i := 0; i < len(os.Args); i++ {
		if i == 1 {
			configPath = configPath + os.Args[i]
		} else if i != 0 {
			configPath = configPath + " " + os.Args[i]
		}
	}
	config, err := getConfig(configPath)
	if err != nil {
		log.Fatal(err)
	} else {
		//Initialize the Database
		err = DbInit(config.ConnectionString)
		if err != nil {
			log.Fatal(err)
		}

		//Start the API Server
		router := NewRouter()
		log.Fatal(http.ListenAndServe(":8090", router))
	}
}
