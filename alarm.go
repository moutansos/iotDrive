package main

type Alarm struct {
	ID         int      `json:"id"`
	GUID       string   `json:"guid"`
	DaysOfWeek []string `json:"daysOfWeek"`
	EveryDay   bool     `json:"everyDay"`
	Hour       int      `json:"hour"`
	Minute     int      `json:"minute"`
}

type Alarms []*Alarm

//TODO: Validation method for alarm
func alarmIsValid(guid string, DaysOfWeek []string, Hour int, Minute int) error {
	return nil
}
