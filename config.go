package main

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	ConnectionString string
}

func getConfig(path string) (*Configuration, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(file)
	config := new(Configuration)
	err = decoder.Decode(&config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
