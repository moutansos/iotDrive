package main

import "fmt"

func AuthorizeKey(key string) error {
	dbKey, err := DbFindAPIKeyByKeyVal(key)
	if err != nil {
		return err
	}
	if dbKey.KeyVal == key && dbKey.Enabled {
		return nil
	}
	return fmt.Errorf("Invalid API key!")
}
